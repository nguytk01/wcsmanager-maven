This is Java Swing application to track water sample collection sites along the St. Joe river's portion that is close to Fort Wayne.

Branch eclipse is the active branch at the moment. The branch master does not receive all changes from the branch eclipse.

# How to build
For Windows users:

* do a git clone of this repository
* run the file maven-env-cmd.bat to open a command line window with JAVA_HOME environment variable pointing to your JDK directory. This is required in order for maven scripts to work.
* change the current directory to the repository's directory if it is not.
+ run the command "git checkout eclipse" to switch to the eclipse branch.
* this command you should do in a Git Bash window or the cmd if you have access to git tools in windows cmd.
- run "mvnw.cmd package" to build everything and give you a .jar file
+ the .jar file should be in wcsmanager\view\target\
- double-click the .jar file to run the application.
